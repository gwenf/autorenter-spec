# AutoRenter Style Guide - JavaScript

This style guide is based mostly on the following:

+ [Google's JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html)

In this document, rather than repeating information available in these and other similar resources, or discussing rules that are automatically enforced by our build process, we will focus on topics where we've had to resolve an ambiguity or disagreement.

## Managing the `this` pointer

If you must cache `this` (e.g., for use in a callback), use the variable name `self`. Although the convention of using 
`that` is common in the JavaScript community, it's confusing. In all other contexts in the real world, "this" and "that"
are two different things. Let's not confuse the two...

## Naming

### Type naming

* Classes and interfaces are named using ```PascalCase```.
* Other types are named using ```camelCase```.

### Other naming

* Variables that are considered constants should be UPPER\_SNAKE\_CASE

``` javascript
var DEFAULT_PORT = 3000;
var port = process.env.PORT || DEFAULT_PORT;
```

* Do *not* prefix names with an underscore (`_`) unless required.
	* For example, certain third-party functions begin with an underscore, so overrides must also begin with an underscore.

```javascript
// Bad
var _location;

// Good
var location;
```
