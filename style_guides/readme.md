# Style Guides

The style guides provide guidance for things that are

* Specific to our company
* Specific to the project
* Without a clear convention in the development community
* Not typically/easily enforced by code analysis tools (e.g., linters)

Some style guides are included in the project-specific repositories. Others, which are relevant to multiple projects, are included here:

* [General](./style_guide_general.md)
* [JavaScript](./style_guide_javascript.md)

Please refer to the appropriate style guide(s) to help ensure consistent and properly written code.
