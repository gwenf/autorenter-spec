# AutoRenter Style Guide - General

In this document we will focus on language and framework independent topics where we've had to resolve an ambiguity or disagreement.

The most important thing to remember is to be consistent. As stated in
[Google's JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html):
> If you're editing code, take a few minutes to look at the code around you and determine its style. If they use spaces
> around all their arithmetic operators, you should too. If their comments have little boxes of hash marks around them,
> make your comments have little boxes of hash marks around them too.
> The point of having style guidelines is to have a common vocabulary of coding so people can concentrate on what you're
> saying rather than on how you're saying it. We present global style rules here so people know the vocabulary, but local
> style is also important. If code you add to a file looks drastically different from the existing code around it, it
> throws readers out of their rhythm when they go to read it. Avoid this.

## Naming

### File naming

* The file name should match the name of the [primary] type defined in the file.
* Test file names should be based on the naming convention of the test target's file. See the project-specific style guide for more details on file name and folder location.

### Other naming

* Don't use abbreviations, unless it's a standard, well-established abbreviation.
* Please, unless it's for use in a simple non-nested loop, **no single-character variable names!**

```javascript
// Bad
var v; // a vehicle
var lnbr; // the number of locations

// Good
var vehicle;
var locationCount;
```

## Inline comments

These should be rare. The code should be self-documenting. Most inline comments just add noise, as in the following example:

``` javascript
// Bad
clearUser(): void {
    // Log the operation
    this.$log.debug("Clearing user...");
    
    // Remove the current user from local storage
    this.localStorageService.remove(this.USERKEY);
    
    // set the user
    // to null
    this.user = null;
}
```

Where inline comments are necessary, please write using proper sentence structure; i.e., begin with an uppercase letter and end with a period or question mark.

## Testing

### What to test

* If you change functionality, a test should break. If no test breaks, then the code has inadequate test coverage.
* If you need to fix a bug, prove that it's fixed by adding a test(s).

### Structuring tests

* Test names/descriptions should be descriptive enough so that any developer (or tester, for that matter) can read and understand what is being tested... without looking at the code. In this example, the nested ```describe``` and ```it``` methods form descriptive sentences:

* "Token Interceptor isApiRequest returns false if not an api request", and
* "Token Interceptor isApiRequest returns true if is an api request":

``` javascript
// Good
describe("TokenInterceptor", () => {

    // Details omitted for clarity...

    describe("isApiRequest", () => {

        it("returns false if not an api request", () => {
        	...
            expect(actualResult).to.be.false;
        });

        it("returns true if is an api request", () => {
        	...
            expect(actualResult).to.be.true;
        });

    });
```

* Try to have only one expectation per test. The description of the test should be consistent with the expectation.
