# What are these files?

Answer:

These txt files were used by a tool (see [PlantUML.com](http://plantuml.com "Plant UML") and [PlantText.com](http://planttext.com)) to generate diagrams. You can just use the online tool to generate the diagrams; no need to install anything!

The html files were used by a different tool [draw.io](http://draw.io) to generate diagrams. Just use the online tool; no need to install anything!