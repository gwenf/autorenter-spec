# Program Modules

** Reference Application functionality is segmented into "modules" of functionality. **

Yes, the word "module" is heavily overloaded. In the present sense we are using it to refer to a cohesive set of business functionality. Please refer to the module descriptions to gain an understanding of what you are to be building and/or testing:

* [Main Module (Shell)](./main_module/main_interactions.md)
* [Admin Module](./admin_module/admin_interactions.md)
* [Fleet Module](./fleet_module/fleet_interactions.md)
* [Tech Support Module](./tech_support_module/tech_support_interactions.md)
