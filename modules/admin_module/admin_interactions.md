# Admin Module

This chapter describes the application's Admin module.

__The Admin module will be stubbed in only to demonstrate navigation and context-sensitive menuing. *However, it will not be fully implemented because it doesn't demonstrate anything that isn't already demonstrated in the Fleet module.*__

## Users

### Landing Page for Admin Module: Users

![Users](./users.png)

Requests made to ```https://thehost.com/admin``` will redirect to ```https://thehost.com/admin/users```, the landing page for the Admin UI.

NOTE:

* Default sort order is by username (asc).
* Only the username column is sortable.

## Branding

![Branding](./branding.png)

Summary of control states:

| Control               	| Required  | State |
| ------------------------- | --------  | ----- |
| Company Name				| Y         |		|
| Company Logo				|			|		|
| Company Logo Upload		|			|		|
| Landing Page Image		|           |		|
| Landing Page Image Upload	|           |		|
| Marketing Text			| Y			|		|
| Submit					|           | +		|
| Cancel					|           | -		|

Key:

* R = Readonly.
* D = Disabled.
* H = Hidden.
* \+ = Enabled only if page is valid.
* \- = Enabled only if data is "dirty".

NOTE:

* Company Logo image dimensions: 300 x 50 px.
* Landing Page image dimensions: 800 x 600 px.
* Submit will update data without performing any navigation.
* Cancel will refresh the page with data from the database.