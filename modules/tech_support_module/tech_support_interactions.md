# Tech Support Module

This chapter describes the application's Tech Support module.

This functionality would not typically appear in an actual app, and if it did, access would be restricted to technical personnel.

To keep things simple, AutoRenter only requires the user to be authenticated to access this functionality.

## Tech Support

### Landing Page for the Tech Support Module

![Tech Support](./tech_support.png)

| Item				| Description							|
|-------------------|---------------------------------------|
| API				| Fetches from the `/api` root path.	|
| Notify			| Presents a UI notification message of the specified level, and logs the message via the API.	|
| Test Exception	| Tests the UI exception handling functionality by throwing an exception.	|
| Server Error		| Tests handling of server-side errors by issuing a GET request to the `/api/raise-error` path.	|