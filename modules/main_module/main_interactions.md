# Main Module

This chapter describes the application's Main module, or "shell."

## Log In {#login}

![Log In](./login.png)

If the user has not yet authenticated, he will be redirected to here from
 
* https://thehost.com
* https://thehost.com/*

NOTE:

* All fields are required.

## Landing Page {#landing}

![Landing Page](./landing_page.png)

This is the main AutoRenter project's landing page. Users navigating to the project root (https://thehost.com) will be redirected to here; unauthenticated users will first be directed to the Log In page.

NOTE:

* From this page the user may navigate to a specific module by any of the following: 
	* Using the Module Menu - navigates to the selected module
	* Clicking on the main graphic - navigates to the Fleet module

## Log Out {#logout}

Logging out is accomplished via the user menu, which is located in the application's header:

![Log Out](./user_menu.png)

Logging out will redirect the user to the Log In page.
