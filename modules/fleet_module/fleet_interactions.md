# Fleet Module

This chapter describes the application's Fleet module.

## Locations

### Landing Page for Fleet Module: Locations {#locations}

![Locations](./locations.png)

NOTE:

* Default sort order is by Site ID (asc).
* Only the Site ID column is sortable.

### Location Detail {#locationDetail}

![Location Detail](./locations_add.png)

Summary of control states:

| Control               | Required  | Add   | Edit  | View  |
| --------------------- | --------  | ----- | ----- | ----- |
| Site ID				| Y         |       |		| R     |
| Name					| Y			|       |       | R     |
| City					|           |       |       | R     |
| State					|           |       |       | R     |
| Submit                |           | +     | +     | H     |
| Cancel                |           |       |       | H     |

Key:

* R = Readonly.
* D = Disabled.
* H = Hidden.
* \+ = Enabled only if page is valid.

## Location Vehicles Configuration Page {#locationVehicles}

![Location Vehicles](./location_vehicles.png)

Used to add, edit or delete Vehicle objects associated with the Location.

NOTE:

* Default sort order is by VIN (asc). 
* Only the VIN column is sortable.

## Location Vehicle Detail Page {#vehicleDetail}

![Location Vehicle Detail](./location_vehicles_add.png)

Summary of control states:

| Control               | Required  | Add   | Edit  | View  |
| --------------------- | --------  | ----- | ----- | ----- |
| VIN					| Y			|		|		| R		|
| Make					| Y			|		|		| R		|
| Model					| Y			|		|		| R		|
| Year					| Y			|		|		| R		|
| Miles					|			|		|		| R		|
| Color					|			|		|		| R		|
| Rent to Own			|			|		|		| R		|
| Submit                |           | +     | +     | H     |
| Cancel                |           |       |       | H     |

Key:

* R = Readonly.
* D = Disabled.
* H = Hidden.
* \+ = Enabled only if page is valid.

## Reports {#reports}

__The Reports page will be stubbed in only to demonstrate navigation and context-sensitive menuing; *no reports will actually be implemented.*__

![Reports](./reports.png)
