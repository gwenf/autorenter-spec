# Overview

The AutoRenter problem domain is fleet management for rental car companies.

The specification contained in this repository is to be used as part of a technical training program for developers. It is not "real-world" enough to be useful to actual fleet managers.

# Purpose

***The primary purpose of AutoRenter is to serve as a training tool for developers.***

## Demonstrate Proper Implementation

The AutoRenter Reference Applications demonstrate proper implementation of features common to many modern Web applications and APIs, including error handling, testing, etc. This is in contrast to the "You wouldn't do this in production, but for demonstration purposes..." shortcuts typical in demo apps.

The Reference Applications may be combined in various ways to create complete, multi-tier, production-ready *Reference Systems*:

![Reference Systems](./images/reference_applications.png)

## Demonstrate Technical Documentation

The scope of AutoRenter extends beyond demonstrating proper development practices; it also illustrates excellent technical documentation and related practices. For example, AutoRenter documentation:

* Was created using free, standards-based tools
* Is maintained under version control
* Is hosted (on GitBook)
* Includes architectural and design diagrams
* Describes the system functionality in a testable manner

## Accelerate Learning

A benefit of our "single spec, multiple implementations" training approach is accelerated translation of skills from one technology to another. For example, many of us have used Google Translate to help us express something in an unfamiliar language based on how we know to express it in our native language...

![Translation](./images/google_translate.jpg)

Our training approach allows the developer to focus on the technology without the additional cognitive load of juggling problem domains (e.g., Joe understands a TODO app, which uses Angular, and is now trying to learn React by studying a financial calculator app implemented with React). After all, who would go to Google Translate to learn how to translate one phrase in English to a completely different phrase in German?

# Features

At a high level, the capabilities demonstrated by a fully-implemented Reference System include:

* Automated tests (unit, integration).
* Context-sensitive menuing.
* Continuous integration, continuous deployment (CI/CD).
* CRUD.
* Error handling.
* Image in grid.
* Logging (for tech support personnel).
* Modal dialog service.
* Navigation verification (to prevent accidental data loss).
* Notifications.
* Sorting.
* Token-based authN/authZ.
* Validation.

In addition, each system includes exemplary technical documentation.
