# Deployment

Each implementation of this spec is developed under the continuous integration / continuous deployment development practice summarized below:

![Deployment](./deployment.png)

Refer to an individual project's implementation for details pertaining to its CI/CD chain; technical details will vary by project type and hosting platform.
