# Security Model

This chapter describes the security model for AutoRenter implementations.

## User Authentication and Authorization {#authNauthZ}

All users will be required to have a JSON Web Token (JWT) attached as an `Authorization` request header. For example:

```
{
    "alg":"HS256",
    "typ":"JWT"
},
{
    iss: process.env.JWT_ISSUER,
    aud: process.env.JWT_AUDIENCE,
    username: user.username,
    sub: user.id,
    iat: Math.floor(Date.now() / 1000),
    nbf: Math.floor(Date.now() / 1000),
    exp: Math.floor(Date.now() / 1000) + (60 * 60 * process.env.JWT_DURATION_IN_HOURS)
}
```

Read more about JWT [here](http://jwt.io).

In this simple model, application access is granted upon receipt of a valid token as summarized in the following sequence diagram:

![Auth Sequence Diagram](./auth_sequence.png)

## Sample Credentials {#credentials}

Users can log in to any of the reference applications using one of these username/password combinations:

* janesmith / secret
* bobsmith / secret
* melissajones / secret
* georgejones / secret
