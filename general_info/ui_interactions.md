# General Information

The following sections describe feature functionality that is consistent throughout the application. We describe it here so we don't have to repeat it elsewhere.

## Target Viewport Size

The minimum supported viewport size is 1280x800 pixels.

## Standard List Page Features

* Context-sensitive Header.
* Sorting. Clicking the header of a sortable column will toggle ascending/descending.
* Delete operations require user confirmation.

## Detail Pages

Most detail pagess are available in Add, Edit, and View modes.

The URL pattern is illustrated in the following Vehicle Detail Page example:

| Mode	  | URL	   |
| ------- |------- |
| Add		|...com/fleet/locations/{locationId}/vehicles/add |
| Edit	|...com/fleet/locations/{locationId}/vehicles/{vehicleId}/edit |
| View	|...com/fleet/locations/{locationId}/vehicles/{vehicleId}/view |

## Navigation Prompt

In add or edit mode, clicking on a main menu item or breadcrumb (or using browser navigation) will implicitly begin canceling the pending add/edit operation. The system will require the user to confirm the cancellation so data entry efforts are not lost.

## Modal Dialogs

The background will not be completely opaque when a modal dialog is being presented; the background page will be partially visible.
 
## Scrolling

Some of the pages have more content than can be shown within the confines of the mockup page dimensions. To ensure that all of the information is shown in the design documentation, the content is allowed to exceed boundaries where necessary. This is a design document convention only; scrolling (or browser resizing) is available in context of the actual application.

![Scrolling Content](./scrolling.jpg)

## "Toast" Messages

Toast messages of the `success` severity level will automatically timeout (close) after 5 seconds.

Unless otherwise noted, toast messages of other severities must be closed by the user.

*Note that the timeout countdown stops if the user interacts with the toast (e.g., hovers over it). Unless the user manually closes the toast, countdown restarts when the user stops interacting with the toast.*


## Secure Access to Features

The system will prevent users from accessing data and functionality they have not been authorized to use. This will be accomplished by (as most appropriate):

* Preventing access to restricted pages
* Removing restricted elements from the page
