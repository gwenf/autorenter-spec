# AutoRenter API

This chapter describes the AutoRenter API.

# Supported HTTP Status Codes {#codes}
 
| code	| verbs		| comments	|
| -----	| ---------	| ---------	|
| 200	| GET, PUT	| OK.		|
| 201	| POST		| Created.	|
| 204	| DELETE	| No Content.	|
| 400	| POST, PUT	| Bad Request.	|
| 401	| GET		| Unauthorized. Requires authentication. |
| 404	| GET		| Used for Forbidden (normally 403, but 404 is more "secure") and Not Found. |
| 500	| GET		| Server Error.	|
 
# Security {#security}

Unless otherwise noted, all routes are secured. See the [Security Model](../security_model/security_model.md) for more details.

# User {#user}

## User Web Model

Definition of the User Web Model.
 
| field name		| type		| comments	  |
| ------------- | ------- | --------	  |
| id				    | string	| Calculated.	|
| userName			| string	|			        |
| password			| string	| Hashed.	    |
| firstName			| string	|			        |
| lastName			| string	|			        |
| token			    | JWT	    |	Only included if authenticated.	|

# Location {#location}

## Location Web Model

Definition of the Location Web Model.
 
| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| id				| string	| Calculated.	|
| siteId			| string	|			|
| name			    | string	|			|
| vehicleCount		| number	| Calculated. Not persisted.|
| city			    | string	|			|
| stateCode	        | string	|			|

## Create Location

Creates a new location. 

**Route**

/api/locations

**Verb**

POST

**Request Body**

Contains a Location object.

**Response Body**

Empty.

**Response Headers**

* Location: URI for the new resource.

## Get Location

Gets detailed information describing the specified location.

**Route**
 
/api/locations/{locationId}
 
**Verb**
 
GET
 
**Request Body**
 
N/A
 
**Response Body**

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| data				| Location	|			|

## Get Locations

Get information for all locations.

**Route**

/api/locations

**Verb**

GET

**Request Body**

N/A

**Response Body**

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| data				| Location[]| Collection of Location objects.	|

## Modify Location

Modifies the specified location.

**Route**

/api/locations/{locationId}

**Verb**

PUT

**Request Body**

Contains a Location object.

**Response Body**

Empty.

**Response Headers**

* Location: URI for the modified resource.

## Delete Location

Deletes the specified location.

**Route**

/api/locations/{locationId}

**Verb**

DELETE

**Request Body**

N/A

**Response Body**

N/A

# Vehicle {#vehicle}

## Vehicle Web Model

Definition of the Vehicle Web Model.
 
| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| id				| string	| Calculated.	|
| vin	            | string    |			|
| makeId			| string	|			|  
| make              | string	| Calculated. Only included in "get all" operations that return the list. |
| modelId			| string	|			|
| model             | string	| Calculated. Only included in "get all" operations that return the list. |
| year              | number	|			|
| miles             | number    |			|
| color             | string	|			|
| isRentToOwn       | boolean 	|			|
| image             | base64 encoded string	|	|

## Create Vehicle
 
Creates a new vehicle for the specified location.
 
**Route**
 
/api/locations/{locationId}/vehicles
 
**Verb**
 
POST
 
**Request Body**

Contains a Vehicle object.
 
**Response Body**
 
Empty.

**Response Headers**

* Location: URI for the new resource.
 
## Get Vehicle
 
Gets detailed information describing the specified vehicle.
 
**Route**
 
/api/vehicles/{vehicleId}
 
**Verb**
 
GET
 
**Request Body**
 
N/A
 
**Response Body**

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| data				| Vehicle	|			|

## Get Vehicles

Get information for all of the specified location's vehicles.

**Route**

/api/locations/{locationId}/vehicles

**Verb**

GET

**Request Body**

N/A

**Response Body**

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| data				| Vehicle[]	| Collection of Vehicle objects.	|

## Modify Vehicle

Modifies the specified vehicle.

**Route**

/api/vehicles/{vehicleId}

**Verb**

PUT

**Request Body**

Contains a Vehicle object.

**Response Body**

Empty.

**Response Headers**

* Location: URI for the modified resource.

## Delete Vehicle

Deletes the specified vehicle.

**Route**

/api/vehicles/{vehicleId}

**Verb**

DELETE

**Request Body**

N/A

**Response Body**

N/A

# Sku {#sku}

## Sku Web Model

Definition of the Sku Web Model.

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| makeId			| string	|			|
| modelId			| string    |			|
| year              | number	|			|
| color             | string	|			|

## Get Skus
 
Get information for all of the system's Skus.
 
**Route**
 
/api/skus
 
**Verb**
 
GET
 
**Request Body**
 
N/A
 
**Response Body**

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| data				| Sku[]		| Collection of Sku objects.	|

# Lookup Data {#lookup}

There are various types of lookup data, all of which are accessed via a common base route. Query parameters are used to specify which kind(s) of lookup data to fetch.

## State Web Model

Definition of the State Web Model.

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| stateCode			| string	|			|
| name				| string    |			|

## Make Web Model

Definition of the [Vehicle] Make Web Model.

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| id				| string	|			|
| name				| string    |			|

## Model Web Model

Definition of the [Vehicle] Model Web Model.

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| id				| string	|			|
| name				| string    |			|

## LookupData Web Model

Definition of the [dynamic] LookupData Web Model, which aggregates collections of lookup data.

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| states			| State[]	| Only available if requested in query params.|
| makes				| Make[]    | Only available if requested in query params.|
| models			| Model[]	| Only available if requested in query params.|

## Get Lookup Data
 
Get the specified kind(s) of lookup data.
 
**Route**
 
/api/lookup-data?value1&value2...

> Example: `/api/lookup-data?makes&models`
 
**Verb**
 
GET
 
**Request Body**
 
N/A
 
**Response Body**

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| data				| LookupData| LookupData object.|

# Log In {#login}

Log in to the system.

**Route**

/api/login

**Verb**

POST

**Request Body**

| field name		| type		| required	| comments	|
| ---------------	| --------	| --------	| ---------	|
| name			    | string	| Y			|			|
| password	    | string	| Y	    	|	Hashed.		|

**Response Body**

User object. 

# Log {#log}

Log diagnostic data. This route is not secured.

**Route**

/api/log

**Verb**

POST

**Request Body**

| field name		| type		| required	| comments	|
| ---------------	| --------	| --------	| ---------	|
| message		    | string	| Y			|			|
| level				| string	| Y			| error, warn, info, debug |

**Response Body**

Empty. 

# API Info {#apiInfo}

Get info about the API. This route is not secured.

## ApiInfo Web Model

Definition of the ApiInfo Web Model.

| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| title				| string	|			|
| environment		| string	|			|
| version			| string	|			|
| build				| string	|			|

**Route**

/api/

**Verb**

GET

**Request Body**

N/A

**Response Body**


| field name		| type		| comments	|
| ---------------	| --------	| --------	|
| data				| ApiInfo	| ApiInfo object.|

# Raise Error {#raiseError}

Triggers an internal server error. Useful for testing handling of errors.

**Route**

/api/raise-error

**Verb**

GET

**Request Body**

N/A

**Response Body**

N/A
